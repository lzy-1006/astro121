\documentclass[12pt, letterpaper]{article}

%% Language and font enco
\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{siunitx}

%% Sets page size and margins
\usepackage[letterpaper,top=2.54cm,bottom=2.54cm,left=2.54cm,right=2.54cm,marginparwidth=1.75cm]{geometry}

%% Useful packages
\usepackage{multicol,caption}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\newenvironment{Figure}
  {\par\medskip\noindent\minipage{\linewidth}}
  {\endminipage\par\medskip}

% \usepackage[
% %   subtle
%   moderate
% ]{savetrees}

\setlength {\marginparwidth}{2cm}

\title{X-Band Radio Interferometry on M17 (Official Submission)}
\author{Ziyi Lu, of Team Rocket}

\begin{document}
\maketitle

\setlength{\abovedisplayskip}{10pt}
\setlength{\belowdisplayskip}{10pt}

\begin{abstract}
An interferometre is an observing system that utilises information from  multiple telescopes. In this project, we use a multiplying two-telescope interferometer to observe point source M17 and have found a feature in the data that could be a fringe described by equations of fringe amplitude and frequency. We then fit the equations to the data to find out that our baseline is $B_ew = (14.19 \pm 0.03)\si{\metre}$ in the east-west direction and $B_ns = (1.58 \pm 0.02)\si{\metre}$ north-south. The fit parameters reproduce the spectral behaviour of the data, but do not seem to have a corresponding waveform. Meanwhile, the fringe frequency equation does not precisely predict the frequency of the fringe amplitude waveform as it should, probably because it unrealistically assumes that hour angle is close to zero.
In the future, we can lower the system noise for better results, and observe various other targets at different hour angles and declinations to test accuracy of our fitted parameters and the generality of the models.
\end{abstract}

\begin{multicols}{2}

\section{Note}
You have to take my words for it, but this is basically the state of this report on the morning of 2020-4-8, Wednesday. I went on to write around 10 pages of excessive content after it till 2020-4-11, largely due to emotional instability, the day of submission, but have decided to trim them in this official submission. You may find that extended version in the same mail of submission.

I request to have this evaluated as if submitted after its completion, i.e. on 2020-4-8, Wednesday morning. If not, please assess the extended version submitted on 2020-4-11. Thank you.

\section{Introduction}

An interferometre is an observing system that utilises information from  multiple telescopes. As the telescopes are in different positions and have different distances to the source, each will have a different geometric delay $\tau$, the time it receives the signal $v(t)$ after the source emits the signal. Its baseline $b$ is the farthest distance between two of its components. A fringe is a sinusoidal-like signal output by an interferometre. \cite{lab3}

Visibility $V$ is a measure of the clarity of the signal. It is defined as the Fourier transform of the correlation of two signals received by two telescopes.
\begin{equation}
    V_{12}(\nu) \equiv \mathbb{F} (v_1 \cdot v_2^{\ast})
\end{equation}
The visibility of two copies of the same signal with different time delays would have a spike, meaning the signal is very clear. In contrast, the visibility of two unrelated pieces of random noise would be low, showing the absence of any clear signal. \cite{lab3}
For baseline $\vec{b}$, the visibility is
\begin{equation}
    V(\vec{b}, \nu) = \int_{Sky} A(\hat{s}) I(\hat{s}) exp(\frac{-2\pi i\vec{b} \cdot \hat{s}}{\lambda} d\Omega)
\end{equation}
where $A$ is a function of the shape of the source in the sky, $\hat{s}$ is the direction to the source, $I$ is intensity, $\lambda$ is the wavelength, and $\Omega$ is the solid angle. For higher source declination DEC, $\vec{b} \cdot \hat{s}$ would be larger, so the fringes in the visibility time series would have longer wavelength. \cite{lab3}
The form of the function, an integration over functions $A(\hat{s}) I(\hat{s})$ multiplied by exponential $exp(\frac{-2\pi i\vec{b} \cdot \hat{s}}{\lambda}$, is equivalent to a Fourier transform of $A(\hat{s}) I(\hat{s})$, the multiplication of the source shape function and intensity over the whole sky. Therefore, the inverse Fourier transform of the visibility time series is the intensity distribution. \cite{lab3}

Any two telescopes in the same interferometre have different distances from the target, ranging from zero to the baseline length. That divided by the speed of light is the geometrical path delay $\tau_g(h_s)$, a function of the hour angle $h_s$ of the source.
\begin{equation} \begin{split}
    \tau_g(h_s) = \tau_{g, ew} + \tau_{g, ns} \\
    = (\frac{B_{ew}}{c} \cos\delta\sin h_s) \\
    + (\frac{B_{ns}}{c} \sin L \cos \delta \cos h_s - \frac{B_{ew}}{c} \cos L \sin\delta)
\end{split} \end{equation}
\noindent where subscripts $ew$ and $ns$ designate east-west and north-south components respectively, $c$ is the speed of light, $L$ is the terrestrial latitude of the interferometre, and $\delta$ is the target declination. We define the hour angle dependent part of $\tau_g$ as $tau_g'$:
\begin{equation} \begin{split}
    \tau_g'(h_s)
    \equiv (\frac{B_{ew}}{c} \cos\delta\sin h_s) \\
    + (\frac{B_{ns}}{c} \sin L \cos \delta \cos h_s)
\end{split} \end{equation}
Meanwhile, there is also relative cable delay $\tau_c$ in the differing electronics signals in them go through, which is constant through time. The total relative delay $\tau_t$ is their sum,
\begin{equation}
    \tau_t = \tau_g + \tau_c = = \tau_g' + \tau_c'
\end{equation}
\noindent where the $B_{ns}$-modified cable delay $\tau_c'$ absorbes $\tau_g$'s' hour angle independent term, i.e.
\begin{equation}
    \tau_c' \equiv \tau_c - \frac{B_{ew}}{c} \cos L \sin\delta
\end{equation}

For a multiplying interferometre, the signals from the telescopes $E_i$ are multiplied together with a DSB mixer to fringe amplitude $F = \prod E_i$. For a two-telescope interferometre, if redshift is negligible, the inputs will be
\begin{equation} \begin{split}
    E_1(t) = \cos (w\pi\nu t)
    E_2(t) = \cos (w\pi\nu [t + \tau_t])
\end{split} \end{equation}
\noindent so that
\begin{equation} \begin{split}
    F(h_s) = \cos (w\pi\nu t) \cos (w\pi\nu [t + \tau_t]) \\
    = \frac{1}{2} \cos (2\pi\nu\tau_t) + F_{2t+\tau_t} \\
    = \cos(2\pi\nu\tau_c')\cos(2\pi\nu\tau_g') -\\ \sin(2\pi\nu\tau_c')\sin(2\pi\nu\tau_g')
\end{split} \end{equation}
\noindent where $F_{2t+\tau_t}$ varies quickly with time and averages to zero, so we ignore that term.

We try to find the baseline $\vec{B}$ with least-squares, so we define
\begin{equation} \begin{split}
    A = \cos(2\pi\nu\tau_c') \\
    B = -\sin(2\pi\nu\tau_c')
\end{split} \end{equation}
\noindent and fit
\begin{equation}
    F(h_s) = A\cos(2\pi\nu\tau_g') + B\sin(2\pi\nu\tau_g')
\label{eq:amp} \end{equation}
\noindent to data, assuming $A$ and $B$ are independent. In this equation,
\begin{equation}
    \nu\tau_g' = (\frac{B_{ew}}{\lambda} \cos\delta\sin h_s) + (\frac{B_{ns}}{\lambda} \sin L \cos \delta \cos h_s)
\end{equation}
\noindent where $\lambda$ is the wavelength.

The fringe amplitude oscillates along hour angle $h_s$, which is proportional to time, so we define the oscillation frequency $f_f$. Parsons et al. have Taylor expanded the $\sin h_s$ and $\cos h_s$ terms to find its form
\begin{equation} \begin{split}
    f_f = \frac{2\pi}{24 \times 3600}(\frac{B_{ew}}{\lambda} \cos h_s + \frac{B_{ns}}{\lambda} \sin h_s) \\
    = 43200(\frac{B_{ew}}{\lambda} \cos h_s + \frac{B_{ns}}{\lambda} \sin h_s)
\label{eq:f_f} \end{split} \end{equation}
\noindent in units of Hertz, assuming a small hour angle
\begin{equation}
    h_s \sim 0
\end{equation}
\cite{lab3}

Least-squares is a common statistical technique in fitting model parameters to data, and non-linear models often require special care. The reason is that common least-squares algorithms start from initial guess values and slightly change the parameters until converging to a local minimum of $\chi^2$, but non-linear models often have local minima that are far from global ones. Thus, a brute-force method is needed, in which possible values of the parameters are all tested and the combination that produces the minimum $\chi^2$ globally is selected.

We select M17, the Omega nebula, as our target of observation. It appears almost as a point source as its apparent diameter, $11'$, is similar to the fringe spacing, and its flux is concentrated within $20"$. It is also quite bright with a spectral flux density of $500$Jy in the X-band, as it is an HII region with warm ionised gas at temperature $T \sim {10}^4\si{\kelvin}$ and produces free-free radiation like the Sun does. \cite{lab3}

In section 2, we describe the methods to take data from the source. We analyse the data in section 3 and give conclusions and suggest improvements in section 4.

\section{Methods}

We use a two-dish multiplying interferometre operating around frequency $\nu \sim 10.7\si{\giga\hertz}$ in the X band of microwaves, with the wavelength as $\lambda = \frac{c}{\nu} = 2.80\si{\centi\metre}$. We get the target location with the \verb$ugradio$ package. Our baseline is short at $B_{ew} \sim 20\si{\metre}$ and $B_{ns} \sim 0$, so the fringe spacing is
\begin{equation} \begin{split}
    s = \frac{\lambda}{B} = \frac{\frac{3.0 \times 10^8}{10.7 \times 10^6}}{20} \\
    % \sim 1.40
    \sim 5'
\end{split} \end{equation}
\noindent which is larger than most sources, making them look like point sources, except for the Sun and the Moon. Theoretically, the precision of declination measurement can be 100 times more accurate to an angular resolution of $3"$. \cite{lab3}

On 2020-3-25, we obtain the J2000 equatorial coordinates of our target at $(RA, DEC) = \deg{275.2}, -\deg{16.2}$, correct it for precession at the present with the \verb$ugradio$ package, and point both dish telescopes to it from 2:20 PST to 9:05 PST for $6.8$ hours. From the time we calculate he target's hour angle to span $20.03$h to $2.80$h with sufficient precision to make error bars insignificant. However, the hour angles are somewhat far from $0$ unlike the assumption of \eqref{eq:f_f}. The pointing of any telescope also never gets more than $\deg{3}$ away from the computed azimuthal coordinates of the target, so we are confident that they should see the target. The voltage outputs from the telescopes are fed into an HP multimetre, which we plot in Fig \ref{fig:m17} together with its processed versions described in the next section. We set the sampling rate to be $1\si{\hertz}$, and there are $N = 24301$ data points in the end. Therefore, the frequency resolution is $d\nu = \frac{nu_s}{N} = \frac{1}{24301} = 4.1 \times 10^{-5} \si{\hertz}$.
The multimetre also records the time to fractions of a second, so we also neglect uncertainty in time.

\begin{Figure}
    \centering
    \includegraphics[width=\linewidth]{m17}
    \captionof{figure}{\label{fig:m17} Multiplying interferometre signal from M17, which has an unstable non-zero average from fluctuating systematic error in the circuits. We fitted it with a quadratic polynomial that represents in the trend and subtracted it to remove the trend, but the result still has an unsteady trend. We Fourier filter that to preserve only the band with the theoretical frequencies of the fringe, and the end product oscillates just around zero.}
\end{Figure}

\subsection{Data Analysis}
The mean voltage from the multimetre is not zero as fringes should be, probably due to the design of our system with a capacitor constantly giving an unstable non-zero voltage related to the brightness of the input signal. As it never gets too far down, so the telescopes probably were always seeing the target without hitting buildings. We compensate the offset by first subtracting a quadratic fit to the data to get rid of the overall trend, and then Fourier filtering out the lower frequencies.

To get the range of the fringe frequencies $f_f$ to which to Fourier filter, we plug in the approximate baseline as well as the hour angle range to Eq \eqref{eq:f_f}. The result is that $f_f$ should be from $0.025\si{\hertz}$ to $0.050\si{\hertz}$. We Fourier transform the time series to get the spectrum in the upper panel of Fig \ref{fig:m17_spectrum} and zero out the part outside $0.022\si{\hertz}$ to $0.050\si{\hertz}$, as we know the baseline $B_{ew}$ is a bit shorter than $20 \si{\metre}$ and would make the actual fringe frequency $f_f$ lower than expected. Indeed, the preserved band shows bumps above the otherwise continuous noise that is bright at short frequencies but dark at higher frequencies, which we consider the actual signal from M17. The spectrum is symmetric about the power axis because it is the Fourier transform of a real time series.

\begin{Figure}
    \centering
    \includegraphics[width=\linewidth]{m17_spectrum}
    \captionof{figure}{\label{fig:m17_spectrum} Upper panel: Spectrum of the M17 signal, and a Fourier filtered version with a bandpass at the fringe frequency range $f_f$ predicted by theory. It is mostly comprised of continuous noise that is bright at short frequencies but dark at higher frequencies, but with bumps at the fringe frequencies.
    Lower panel: A part of both the original and filtered M17 signal. The filtered version has smaller but similar amplitude, and looks remotely like the smoothened version of the unfiltered one.}
\end{Figure}

The fringe frequency $f_f$ is expected to change with hour angle $h_s$, which changes linearly with time $t$ for such a source approximately fixed on the celestial sphere. To examine that change, we slice the data into $18$ even snippets of length $t_s = 1735\si{second}$, Fourier transform them to get their spectra, and stack them into a spectrogram in Fig \ref{fig:m17_spectrogram}. The colour-bar is power with arbitrary units. The frequency resolution of the spectra should be $\pm\Delta\nu = \frac{1}{1735} = 0.0006\si{\hertz}$.

\begin{Figure}
    \centering
    \includegraphics[width=\linewidth]{m17_spectrogram}
    \captionof{figure}{\label{fig:m17_spectrogram} Spectrogram of the M17 signal, symmetric about the power axis all the time because its time series is always real. The low-frequency noise dominates and fluctuates like the envelope of the time series, but there are also two clear fringe frequency arcs again near the predicted range like the bumps on the spectrum. They have highest frequency near the middle where hour angle is close to $0$ and lower near the ends, like predicted, so we are probably seeing the fringe.}
\end{Figure}

The spectrogram is symmetric about the power axis all the time because its time series is always real. While the low-frequency noise dominates and fluctuates like the envelope of the time series, there are also two clear fringe frequency arcs again near the predicted range, from $0.025\si{\hertz}$ to $0.040\si{\hertz}$, like how the spectrum had the bumps there. They have highest frequency near the middle where hour angle is close to $0$ and lower near the ends, just like predicted by Eq \eqref{eq:f_f}. That means that we are probably seeing the fringe, but we must confirm by comparing the whole expected $t$-$f_f$ curve to it. Meanwhile, the maximum frequency is lower than the filter's upper bound, suggesting a frequency overestimate.

To recover the time series of the signal, we inverse Fourier transform the filtered spectrum to get the filtered time series in Fig \ref{fig:m17}. Notably, it is a wave centred around zero with mostly uniform amplitude instead of fluctuating like the original, and the amplitude looks smaller than that of the original, suggesting that noise may have contributed significant brightness.

To examine how much a signal we have in the data, we zoom in on waveforms a portion of unfiltered and filtered versions of the signal in the lower panel of Fig \ref{fig:m17_spectrum}. It is taken from the middle in time and should be least prone to systematic errors near the start and end of data taking. The unfiltered one is too far from zero, making it hard to compare the shapes of the waves, so we subtract the mean of this portion from it to pull it near the filtered one.

The filtered one has smaller but similar amplitude, and looks remotely like the smoothened version of the unfiltered one. The waveform is not as compelling as the bump in the spectrum or the arcs in the spectrogram in confirming that the filtered time series is the M17 signal, but the latter two convince us enough to carry on with the analysis.

We then try to find the baseline lengths by least-square fitting the Fourier filtered time series to fringe amplitude $F$ predicted in \eqref{eq:amp}. As the amplitude varies sinusoidally with baselines, we brute-force fit the data to avoid falling into wrong local minima by iterating over possible ranges for $B_{ew} \in [14\si{\metre}, 20\si{\metre}]$ and $B_{ns} \in [-2\si{\metre}, 2\si{\metre}]$ obtained with trial-and-error, fitting coefficients $A$ and $B$ to $F$ for each iteration, and calculate the reduced $\chi^2_r$'s of $F$ to the time series. We take $240$ iterations per baseline component so that the resolution of $B_{ew}$ would be
\begin{equation}
    \pm\Delta B_{ew} = \frac{20 - 14}{240} = \pm 3\si{\centi\metre}
\end{equation}
\noindent and that of $B_{ns}$ would be
\begin{equation}
    \pm\Delta B_{ns} = \frac{2 - (-2)}{240} = \pm2\si{\centi\metre}
\end{equation}
\noindent both similar to the wavelength $\lambda = 2.8\si{\centi\metre}$, which is probably the maximum possible resolution using signals at this wavelength. There are therefore $240^2$ interations in total. The result is in Fig \ref{fig:m17_fit}.

\begin{Figure}
    \centering
    \includegraphics[width=\linewidth]{m17_fit}
    \captionof{figure}{\label{fig:m17_fit} $\chi^2_r$'s of fits to the fringe amplutude $F$ equation (Eq \eqref{eq:amp}) with the filtered M17 signal. The minimum is at $B_ew = (14.19 \pm 0.03)\si{\metre}$ and $B_ns = (1.58 \pm 0.02)\si{\metre}$, the crossing point of a narrow fan of rays with low $\chi^2_r$, suggesting that the ratio of the components of the baseline need to be within a certain range to fit the data.}
\end{Figure}

The global minimum is at $B_ew = (14.19 \pm 0.03)\si{\metre}$ and $B_ns = (1.58 \pm 0.02)\si{\metre}$, the crossing point of a narrow fan of rays with low $\chi^2_r$, suggesting that the ratio of the components of the baseline need to be within a certain range to fit the data. There are other lines parallel to them on the side, and another local minimum close to the global minimum with its fan of rays roughly symmetric about a horizontal line.

We then pick the baseline and coefficient values that generate the smallest $\chi^2_r$ and fit them to the time series again to get the most precise result possible. The output is still $B_ew = (14.19 \pm 0.03)\si{\metre}$ and $B_ns = (1.58 \pm 0.02)\si{\metre}$, with $A = 479.16$ and $B = 143.06$, indicating
\begin{equation} \begin{split}
    \tau_c' = \arctan(\frac{-B}{A}) / (2\pi\nu) \\
    = \frac{-0.29 + n\pi}{2\pi \times 10.7 \times 10^9} \enspace \{n \in \mathbb{Z}\} \\
    = (4.24 + 4.67n) \times 10^{-11} \si{\second}
\end{split} \end{equation}

We plug the values back to \eqref{eq:amp} to get the fitted fringe in Fig \ref{fig:m17_fitted}, compared with the original filtered signal. The upper panel shows the whole signal and the lower one shows a zoomed in portion of them together with the unfiltered data. The fitted fringe is constant in amplitude, which looks like the minimum of the fluctuating amplitude of the filtered time series. That may mean that the fitted curve has extracted the theoretically predicted fringe from the signal, ignoring the noise. However, while the zoomed in fitted curve looks somewhat like the filtered one with all but one dominant wavelength removed, it looks different from the unfiltered signal, casting a doubt on how much the fit is related to the original data.

\begin{Figure}
    \centering
    \includegraphics[width=\linewidth]{m17_fitted}
    \captionof{figure}{\label{fig:m17_fitted} The fitted fringe from M17 compared to the original filtered signal.
    Upper panel: The full signal. The fitted fringe is constant in amplitude, which looks like the minimum of the fluctuating amplitude of the filtered time series. That may mean that the fitted curve has extracted the theoretically predicted fringe from the signal, ignoring the noise.
    Lower panel: A $200\si{\second}$ portion of the unfiltered, filtered, and fitted time series. While the fitted curve looks somewhat like the filtered one with all but one dominant wavelength removed, it looks different from the unfiltered signal, casting a doubt on how much the fit is related to the original data.}
\end{Figure}

Before we analyse the fitted time series more, we calibrate the flux density axes of our graphs in this whole project with it, including the magnitudes of previous $A$ and $B$ magnitudes, because the flux density from M17 is known to be stably $500$Jy \cite{lab3}, and the multiplying interferometre outputs a visibility signal proportional to flux density. If we assume the fitted time series to be the pure signal from M17, its amplitude would correspond to $V = 500$Jy, and as it is a sum of two sine waves (dominated by the east-west term of them), the root mean square should be approximately $V_{rms} = \frac{500}{\sqrt{2}}$Jy. As the root means square in our multimetre output is $2.39 \times 10^{-6}\si{\volt}$, we get that $2.10 \times 10^8\si{\volt}$ is equivalent to $500$Jy.

We Fourier transform the fitted wave into the spectrum in Fig \ref{fig:m17_fitted_spectrum} to examine whether it is more than a sine wave, and realise that it is. It is as symmetric about the power axis as that of a real time series should, and limited to a range of $0.019 - 0.037\si{\hertz}$, narrower than that of the filter, suggesting that we did not get the precise upper bound for fringe frequency $f_f$ with the baseline initial guess. It is highest at the original maximum at $0.037\si{\hertz}$ and drops quasi-smoothly with an oscillation toward lower frequencies until its lower bound, slightly lower than the lower bound guessed through trial-and-error. The coincidence of the maxima of the  spectra and the much lower brightness of the filtered spectrum above $0.037\si{\hertz}$ suggest that the filtered signal may have followed the theoretical spectrum of fringe frequency to some extent with the baselines $B_ew = (14.19 \pm 0.03)\si{\metre}$ and $B_ns = (1.58 \pm 0.02)\si{\metre}$. The oscillation may be an artefact from discrete Fourier transformation. The wavelength of the oscillation gets longer at lower frequencies.

\begin{Figure}
    \centering
    \includegraphics[width=\linewidth]{m17_fitted_spectrum}
    \captionof{figure}{\label{fig:m17_fitted_spectrum} The spectrum of the fitted time series. It is limited to a range smaller than that of the filter, suggesting that we did not get the precise upper bound for fringe frequency $f_f$ with the baseline initial guess. It is highest at the original maximum and drops quasi-smoothly with an oscillation toward lower frequencies until $0.019\si{\hertz}$, slightly lower than the lower bound guessed through trial-and-error. The coincidence of the maxima of the spectra suggest that the filtered signal may have followed the theoretical spectrum of fringe frequency with the baselines $B_ew = (14.19 \pm 0.03)\si{\metre}$ and $B_ns = (1.58 \pm 0.02)\si{\metre}$.}
\end{Figure}

To examine the time variation of the fitted wave, we make its spectrogram in Fig \ref{fig:m17_fitted_spectrogram}, again dividing the wave into $1735\si{\second}$ snippets and Fourier transforming them. The colour-bar is power with arbitrary units. The spectrogram is always symmetric about the power axis as that of a real time series should. It has two clean arcs ranging from $0.019 - 0.037\si{\hertz}$, almost perfectly coinciding with the arcs in the spectrogram of the original signal, suggesting a fringe in the signal following theoretical predictions assuming $B_ew = (14.19 \pm 0.03)\si{\metre}$ and $B_ns = (1.58 \pm 0.02)\si{\metre}$.

\begin{Figure}
    \centering
    \includegraphics[width=\linewidth]{m17_fitted_spectrogram}
    \captionof{figure}{\label{fig:m17_fitted_spectrogram} The spectrogram of the fitted time series. The colour-bar is power with arbitrary units. There are two clean arcs ranging from $0.019 - 0.037\si{\hertz}$, coinciding with the arcs in the spectrogram of the original signal, suggesting a fringe in the signal following theoretical predictions assuming the fit baselines.}
\end{Figure}

To analyse the arcs more numerically, we take the frequencies of the maxima in the spectrograms of the original and the fitted waves within $0.019 - 0.037\si{\hertz}$ and plot them in Fig \ref{fig:m17_f_f} together with fringe frequency $f_f$ predictions following \eqref{eq:f_f} with the initial guess and fitted values for the baselines. The predictions look as sinusoidal as they are in Eq \eqref{eq:f_f}. The maximum frequencies should be the fringe frequencies, but all but one lie slightly above the $f_f$ curve unexpectedly with fitted baselines, probably due to the Taylor expansion approximation in the derivation from Eq \eqref{eq:amp} to Eq \eqref{eq:f_f}, as that assumes hour angle $h_s$ close to zero. %FIXME
Nevertheless, many of them coincide with the actual maxima and the others are close to them, suggesting that baselines $B_ew = (14.19 \pm 0.03)\si{\metre}$ and $B_ns = (1.58 \pm 0.02)\si{\metre}$ would indeed produce a fringe similar to the one in our data. The $\chi^2_r$ for the actual maxima frequencies compared to those of the $F$ fit is low at $1.78$. The curve from the initial guess is way higher, however, explaining why the filter upper bound is higher than the spectrum of the spectrum of the fitted wave and the arcs in the spectrogram of the actual signal.

\begin{Figure}
    \centering
    \includegraphics[width=\linewidth]{m17_f_f}
    \captionof{figure}{\label{fig:m17_f_f} The maxima in the spectrograms of the original M17 signal and the fitted waves within the filter range and fringe frequency $f_f$ predictions with the initial guess and fitted values for the baselines. The maxima from fitted data unexpectedly lie slightly above the $f_f$ curve with fitted baselines, probably due to the Taylor expansion approximation in the derivation from Eq \eqref{eq:amp} to Eq \eqref{eq:f_f}. Nevertheless, many of them coincide with the actual maxima and the others are close to them, suggesting that fit baselines would indeed produce a fringe similar to the one in our data. The curve from the initial guess is way higher, however, explaining why the filter upper bound is higher than the spectrum of the spectrum of the fitted wave and the arcs in the spectrogram of the actual signal.}
\end{Figure}

In conclusion, there is a feature in the M17 data that corresponds to theoretical predictions from Eq \eqref{eq:f_f} of frequencies of a fringe assuming baselines $B_ew = (14.19 \pm 0.03)\si{\metre}$ and $B_ns = (1.58 \pm 0.02)\si{\metre}$ with discrepancies probably due to approximations introduced in deriving Eq \eqref{eq:f_f} from Eq \eqref{eq:amp}, as predicted frequencies deviate more from frequencies of a fringe amplitude $F$ wave than from the actual data. While we can fit a $F(h_s)$ time series to the M17 signal with the same frequency range ranging from $0.019 - 0.037\si{\hertz}$ and its hour-angle dependent behaviour, we cannot produce a fit with a waveform convincingly similar to the original.

\section{Conclusion}
In this project, we have used a multiplying two-telescope interferometer to observe point source M17 and found a feature in the data that could be a fringe described by equations of fringe amplitude and frequency, having higher frequency when the hour angle $h_s$ is closer to zero. We then fit the equations to the data to find out that our baseline is $B_ew = (14.19 \pm 0.03)\si{\metre}$ in the east-west direction and $B_ns = (1.58 \pm 0.02)\si{\metre}$ north-south. The fit parameters produce clean replicas of the spectra of the data, but do not seem to have a corresponding waveform. Meanwhile, the fringe frequency equation does not precisely predict the frequency of the fringe amplitude waveform as it should, probably because it unrealistically assumes that hour angle is close to zero.

To make the results more convincing, we can lower the system noise to produce cleaner signals easier to fit to models. We can also observe various other targets, espcially extended sources, at different hour angles and declinations to test accuracy of our fitted parameters and the generality of the models.

\section{Acknowledgements}
Chingham Fong, Giovanni Pecorino, and Kian Shahin are the other members of the group. We take data together.

\bibliographystyle{plain}
\bibliography{references}
\end{multicols}
\end{document}
