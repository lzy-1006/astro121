import numpy as np
import matplotlib.pyplot as plt

B_ew = 20
B_ns = 0
c = 3.0e8

nu = 10.7e9
dec = -16.2 * np.pi / 180
h_s = np.linspace(19.04 - 24, 1.80) * np.pi / 12

lam = c / nu
f_f = (2 * np.pi / 24 / 3600) * np.abs(B_ew * np.cos(dec) * np.cos(h_s) / lam)
print('f_f', np.min(f_f), np.max(f_f))
plt.plot(h_s, f_f)

plt.figure()
plt.hist(f_f, bins=10)
plt.show()
