\documentclass[a4paper]{article}

%% Language and font enco
\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}

%% Sets page size and margins
\usepackage[a4paper,top=2.54cm,bottom=2.54cm,left=2.54cm,right=2.54cm,marginparwidth=1.75cm]{geometry}

%% Useful packages
\usepackage{multicol,caption}
\usepackage{graphicx}
\newenvironment{Figure}
  {\par\medskip\noindent\minipage{\linewidth}}
  {\endminipage\par\medskip}

\usepackage[
%   subtle
  moderate
]{savetrees}

\setlength {\marginparwidth }{2cm}

\title{Experiments on Fundamentals of Radio Astronomy: Sampling, Fourier Transforms, \& Mixers}
\author{Ziyi Lu, of Team Rocket}

\begin{document}
\maketitle

\begin{multicols}{2}
\section{Acknowledgements}
Chingham Fong, Giovanni Pecorino, and Kian Shahin are the other members of the group. We do everything together.

\begin{abstract}
To explore knowledge and tools in information theory, Fourier analysis, and circuit design that are relevant to radio astronomy, we have sampled sine waves, generated noise block, and built Double-Sideband (DSB) and Single-Sideband (SSB) mixers. We have confirmed the Nyquist criterion for sampling single-tone sine waves, the correlation theorem in the case of autocorrelation functions, the fact that longer integration time leads to a higher Signal-to-Noise ratio, and the usefulness and limitations of DSB and SSB mixers. We have also observed power leakage, the effect of wide Nyquist windows, and measured that the frequency resolution for our setup is $5.9$kHz.
\end{abstract}

\tableofcontents

\section{Introduction}
Radio astronomy requires the taking, processing, and understanding of digital signals. Relevant knowledge in information theory, Fourier analysis, and circuit design are essential to those practices. We try to conduct experiments to confirm certain theories and observe the consequences of some common designs.
One important theory is that of the Nyquist criterion, which states that aliasing occurs when the bandwidth of a signal exceeds half of the sampling frequency $\nu_s$. Aliasing means that the signal would be indistinguishable from another signal with a bandwidth lower than that critical frequency $\nu_c = \nu_s / 2$.
Another important concept is the Fourier transform, which writes a time domain function as another form with frequency bases. The inverse Fourier transform does the exact opposite, expressing a frequency domain function in another form in time domain.
\begin{equation}
f(t) \leftrightarrow F(\omega)
\end{equation}
The Nyquist window is the range of frequencies in a Fourier transform, often set to integer times of $\pm \\nu_c$.

\begin{equation}
Nyquist \, window = W \pm \\nu_c \, \|\, W \in \mathbb{N}
\end{equation}
 \noindent \cite{lab1}
In section 2, we describe an experiment in sampling simple sine waves; in section 3, we sample generated noise instead; in section 4, we explore the frequency resolution with multiple combined sine waves; in section 5, we describe the theory and practicalities of using a Double-Sideband mixer; in section 6, we turn to the Single-Sideband mixer and its pros and cons.

\section{Sampling a Sine Wave}

\subsection{Methods}

To confirm the Nyquist criterion, we first use a Keysight N9310A signal generator to generate sine waves at different frequencies and feed them to a Rigol DS1052E digital oscilloscope and then our computers through a co-axial T joint, a PicoSampler 200 Analog-to-Digital Converter (ADC) and then the \verb$ugradio$ package from Aaron Parsons.

The PicoSampler has a fixed sampling frequency $v_s = 62.5 MHz$, so we chose to generator signals of frequency $v_0 = (0.1, 0.2, 0.3, \dots, 0.9) v_s$. We chose to set the amplitude $A$ to always be $0.00dBm$, i.e. around $0.6V$, to be produce clear signals within a $1.0V$ voltage range for the PicoSampler. The equipment all have the option to set the parameters to great precision, so we neglect the random error of those values from this point on in the project.
We took $N = 16000$ samples for each signal. We plot their wave-forms in Fig \ref{fig:Nyquist1-4}.

\begin{Figure}
\centering
\includegraphics[width=\linewidth]{0.1-0.4waveforms.png}
\captionof{figure}{\label{fig:Nyquist1-4} The wave-forms of sinusoidal signals at 0.1-0.9 times of the sampling frequency}
\end{Figure}

\subsection{Data Analysis \& Interpretation}

In our first experiment, the Nyquist window has the same width as double the critical frequency $\nu_c$, so the bandwidth is the maximum frequency of the signal $\nu_0$, which is also its only frequency. The apparent frequency after aliasing would be
\begin{equation}
\nu_{apparent} = |\nu_0 \bmod \nu_s| = \nu_s - \nu_0
\end{equation}

Observing the wave-forms of our signals, we see that $\nu_0 = \nu_c$ indeed divides the results. The waves with $\nu_0 > \nu_c$ seem to have periods similar to their counterparts with $\nu_0' = \nu_s - \nu_0$. Precise measurements of the periods should confirm that impression more firmly.
Meanwhile, the wave with $\nu_0 = \nu_c$ stands on its own with no indistinguishable counterpart. It is also notable that all data-points of the wave-form are peak / trough extrema, containing little information about the shape of the wave.

We also used the \verb$numpy.fft$ package to Fourier transform the waves to power spectra, setting the number of frequencies the same as the number of samples $N = 16000$. We plot the resulting power spectra in Fig \ref{fig:Nyquist1-4FFT}.
\begin{Figure}
\centering
\includegraphics[width=\linewidth]{0.1-0.4fft.png}
\captionof{figure}{\label{fig:Nyquist1-4FFT} The power spectra of sinusoidal signals at 0.1-0.9 times of the sampling frequency}
\end{Figure}
Expectedly, the sinusoidal waves only have two peaks on their power spectra at $\nu_0$ and $-\nu_0$, due to the fact that any real sine wave has two components $e^{\pm ia \nu_0}$ where $a$ is a constant and $i$ is the imaginary unit. Signals of negative frequencies $\nu < 0$ can be explained as waves with their sine components having a phase of $\pi$. Mathematically,
\begin{equation}
Ae^{-i\omega t} = A(\cos(- \omega t) + i \sin( -\omega t)) = A(\cos( \omega t) + i \sin( \omega t + \pi))
\end{equation}
As before, the spectra of waves with $\nu_0 > \nu_c$ coincide with those of the waves with $\nu_0' = \nu_s - \nu_0$, confirming that signals not satisfying the Nyquist criterion would be indistinguishable with other waves. Meanwhile, the frequencies of the signals with $\nu_0 > \nu_c$ have their peak frequencies the same as the 'true' frequencies set on the signal generators.
The spectrum of for $\nu_0 = \nu_c$ is singular: it has only one peak at $-\nu_0$, but not $\nu_0$, and the peak is at the edge of the frequency range. This is probably due to limitations of the intrinsically dicrete \verb$numpy.fft$ functions. Future investigations on the behaviour of the spectrum with different number of frequency bins would be worthy.
Again, precise measurements of the peak frequencies in the spectra would strengthen that conclusion.
The power differs across different frequencies because the power of the generator is known to flicker randomly every time the user enters a new setting. The absolute power does not matter in our experiment, as we are only testing theories about frequency.

In conclusion, the Nyquist criterion is true about that the absence of aliasing requires
\begin{equation}
\nu_s \ge 2|\nu_0
\end{equation}
In contrast to the power spectra, the voltage spectra do not square the results of the Fourier Transform, and therefore can be unintuitively complex. In particular, the Fourier Transform of a real unit sine wave with angular frequency $\omega$ is

\begin{equation}
\sin(\omega t) = \frac{e^{i\omega t} - e^{-i\omega t}}{2i}
\end{equation} \noindent with imaginary amplitude $A_i = \frac{1}{2i}$ and that of a real unit cosine wave is
\begin{equation}
\cos(\omega t) = \frac{e^{i\omega t} + e^{-i\omega t}}{2}
\end{equation} \noindent with real amplitude $A_R = \frac{1}{2}$.

As the cosine wave is just a sine wave phase-shifted by $\frac{\pi}{2}$ and the Fourier Transform decomposes any real signal to sine waves possibly with different phases, some of the components can be imaginary or complex representing a phase similar to a cosine wave. In mathematical form, a component with a phase of $\phi$ and real amplitude $A$ can be represented as
\begin{equation}
Ae^{i\omega t + \phi} = (Ae^\phi)e^{i\omega t} = A_C e^{i\omega t}
\end{equation} \noindent where the complex amplitude
\begin{equation}
A_C = Ae^\phi
\end{equation} contains information about both the real amplitude $A$ and the phase $\phi$.

A purely real component would be an even cosine wave, i.e. a sinusoidal signal starting at its peak or trough. A purely imaginary component would be an odd sine wave, a sinusoidal signal starting at its middle.

We plot the voltage spectra of different samples of the same frequency $\nu_0 = 0.2\nu_s$ taken on different dates with the real and imaginary parts separated on Fig \ref{fig:Nyquist2Voltage}.

\begin{Figure}
\centering
\includegraphics[width=\linewidth]{0.2voltage.png}
\captionof{figure}{\label{fig:Nyquist2Voltage} The voltage spectra of different samples of the same sinusoidal signal}
\end{Figure}

The real parts are always even, symmetric about the $y$-axis, while the imaginary parts are always odd, antisymmetric about the $y$-axis. That can be explained by the facts that cosine waves corresponding to real amplitude with negative frequencies have the same amplitude, i.e.

\begin{equation}
\cos(-\omega t) = \cos(\omega t)
\end{equation} \noindent while sine waves corresponding to imaginary amplitude with negative frequencies have the opposite amplitude, i.e.
\begin{equation}
\sin(-\omega t) = - \sin(\omega t)
\end{equation}


Across different dates, the frequency peaks seem to all be similar and coincide with the intended frequency set on the generator, although we need more precise measurements of the peaks to confirm that result.
Nevertheless, the relative amplitudes of the real and imaginary peaks differ dramatically, though they often are on the same order of magnitude. That is probably due to phase differences of the signals, determined randomly by its initial phase as the sampler begins to take data, as they all have fixed periods.
Some seemingly unintuitive results have simple explanations. When the real parts are negative, there is probably an aforementioned $\pi$ phase difference. When the imaginary part has higher amplitude than the positive one does, it is probably that the odd sine wave component is stronger than the even cosine wave component.

We have also plotted the power spectra of the signal on Fig \ref{fig:Nyquist2Power}.

\begin{Figure}
\centering
\includegraphics[width=\linewidth]{0.2power.png}
\captionof{figure}{\label{fig:Nyquist2Power} The power spectra of different samples of the same sinusoidal signal}
\end{Figure}

The peak frequencies and overall shapes are similar, except for that of the sample on 23 Jan probably due to faults inside the generator. Therefore, power spectra are more useful when examining the frequency domain spectrum of a signal as it ignores phase differences producing different real and imaginary parts on the spectrum, while voltage spectra are more practical when the phase matters.
Again, the power differs across different frequencies because the power of the generator is known to flicker randomly every time the user enters a new setting. It should not matter in our experiment, as the real and imaginary parts in the frequency domain are generated together in the time domain, and only the relative amplitude matters in our experiment about the Fourier transform.

The correlation theorem states that the Fourier transform of the correlation of two functions $f$ and $g$ is the product of their Fourier transforms.i.e.

\begin{equation}
lim_{T \to \infty} f(t)*g(t) \leftrightarrow F(\omega)G(\omega)
\end{equation}
For $f = g$, the correlation becomes an autocorrelation function (ACF). The result of an ACF on a $f(t)$ in the time domain should equal to the inverse Fourier transform of its power spectrum as the sampled time $T$ approaches infinity, i.e.

\begin{equation}
lim_{T \to \infty}ACF(f(t)) = IFFT((F(\omega))^2)
\end{equation}
 \noindent \cite{lab1} \\
\indent We inverse Fourier transform the $\nu_0 = 0.2 \nu_s$ signal from 26 Jan with the \verb$numpy.fft.ifft$ package. We do it in multiple separate ways: first by inverting the power spectrum, then by applying ACFs directly to the raw voltage time series. We try 2 different ACFs: one from \verb$numpy.fft$ and another from \verb$scipy.signal$. The results are plotted as Fig \ref{fig:ACF}.

\begin{Figure}
\centering
\includegraphics[width=\linewidth]{0.2ACF.png}
\captionof{figure}{\label{fig:ACF} The inverse Fourier transform of the power spectra  and the ACFs of the sinusoidal signal, which theoretically should be equal}
\end{Figure}

The power spectrum inverse  has constant amplitude like the original signal, while results of the ACFs have much larger amplitudes and an overall hexagonal shape, probably due to the finite nature of the signal - the part near the ends are farther from other wave sections that are similar and correlatable to them.

We zoom in on the first 20 signals and normalise the waves to examine the wave-forms in Fig \ref{fig:ACFZoomed}. The wave-forms look similar but different. In particular, the inverse looks exactly like the original wave in Fig \ref{fig:Nyquist1-4}, while the ACF wave-forms overlap with each other but differ from the inverse. The possibly different implementations of ACF in \verb$numpy.fft$ and  \verb$scipy.signal$ do not influence the results. Their discrepancy with the inverse is probably due to loss of information in the ACF process, but also possibly because of errors in our handling and \verb$numpy.fft$ and  \verb$scipy.signal$ of the ACF. Still, the frequency and rough shape of the results of ACF look similar to those of the inverse, suggesting that the correlation theorem is true to some degree.

\begin{Figure}
\centering
\includegraphics[width=\linewidth]{0.2ACFZoomed.png}
\captionof{figure}{\label{fig:ACFZoomed} The inverse Fourier transform of the power spectra  and the ACFs of the sinusoidal signal, zoomed in}
\end{Figure}
We plot the absolute values of the Fourier transforms of the ACFs and compare them to the original in Fig \ref{fig:ACFPower}. Except for the power, their shapes look exactly the same. If we zoom in on Fig \ref{fig:ACFPowerZoomed}, the peak shapes still look perfectly identical except for the absolute height.
\begin{Figure}
\centering
\includegraphics[width=\linewidth]{0.2ACFPower.png}
\captionof{figure}{\label{fig:ACFPower} The Fourier transform of the ACFs and the original power spectra}
\end{Figure}


\begin{Figure}
\centering
\includegraphics[width=\linewidth]{0.2ACFPowerZoomed.png}
\captionof{figure}{\label{fig:ACFPowerZoomed} The Fourier transform of the ACFs and the power spectra, zoomed in}
\end{Figure}

Therefore, we conclude that ACF extracts similar information as the power spectrum inverse does, but may lose some information about wave-form or phase. More numerical analyses on and correlation of peak shapes and positions would strengthen that conclusion.
Spectral leakage is a common phenomenon in which the discrete Fourier transform of a signal is non-zero at frequencies theoretically not existent in the signal. \cite{lab1}
To examine it closely, we Fourier transform the signal taken on 23 Jan with another package \verb$ugradio.dft$ so that we may specify the number of frequency bins to be $N_{freq} = 20 \times N = 320000$, much more than the number of samples. The result is Fig \ref{fig:leak}.

\begin{Figure}
\centering
\includegraphics[width=\linewidth]{0.2leak.png}
\captionof{figure}{\label{fig:leak} Supersampled Fourier transform of the sinusoidal signal with Leakage}
\end{Figure}

The previously sharp peaks begin to shape like a $sinc$ function, which is the Fourier transform of a rectangular function. While this may be an artefact of the over-precise super-sampling of frequency, we may also explain it as the Fourier transform of the rectangular envelope of a finite constant amplitude signal. While it is impossible to test whether an infinite signal would yield the same leakage, it would be helpful to apply a window function, e.g. the popular Hamming window, to the signal and see whether the leakage changes.

We also plot the power spectrum of the same Fourier transform on a log scale for power for a better look in Fig \ref{fig:leak_power}. The peaks different from the primary peak are prominent on the power spectrum. The leakage is significant here.

\begin{Figure}
\centering
\includegraphics[width=\linewidth]{0.2leak_power.png}
\captionof{figure}{\label{fig:leak_power} Logarithmic supersampled power spectrum of the sinusoidal signal, with clearer leakage}
\end{Figure}

Apart from looking at the signal at higher precision, we also look at it from a wider Nyquist window. We choose $W = 4$ so that the frequency axis spans over $\pm 2\nu_c$ in Fig \ref{fig:window}.

\begin{Figure}
\centering
\includegraphics[width=\linewidth]{0.2window.png}
\captionof{figure}{\label{fig:window} Power spectrum of the sinusoidal signal, expanded over a Nyquist window $4$ times wider}
\end{Figure}

The shape in Fig \ref{fig:Nyquist1-4FFT} stays in the same position, but is mirrored by the vertical line $\nu = \nu_c$. The reflection image is again reflected by another vertical line $\nu = 3\nu_c$. In other words, the signal is aliased across $\nu = \nu_c$ like the waves with frequency higher than the critical frequency $\nu_0 \> \nu_c$, and the aliased signals are aliased themselves across $\nu = 3\nu_c$.
We expect this to continue throughout the window if we have chosen a wider window with a larger $W$.

\section{Sampling Noise}

Noise is defined to be 'random fluctuations with a known statistical distribution'. In contrast to popular connotations, it is not always supposed to be ridden of, but can be the signal of interest in astronomy.
Johnson noise, nevertheless, is one type that often contributes to error. It is defined to be the noise generated by random thermal movement of charge carriers, which are in our circuit in our case. \cite{lab1}

We explore the properties of noise by generating and examining Gaussian signals.

\subsection{Methods}

Similar to the setup in the last section, we first use a Micronetics NOD5250 generator to generate the noise, feed it to a MiniCircuits SBP-21.4 bandpass filter that is $6MHz$ wide, a Rigol DS1052E digital oscilloscope, and then our computers through a co-axial T joint, a PicoSampler 200 Analog-to-Digital Converter (ADC), and then the \verb$ugradio$ package from Aaron Parsons
We have generated 32 blocks of noise, each with $N = 16000$ samples. One wave-form is plotted in Fig \ref{fig:noise_wave-form}.

\begin{Figure}
\centering
\includegraphics[width=\linewidth]{noise.png}
\captionof{figure}{\label{fig:noise_wave-form} wave-form of a noise signal}
\end{Figure}

\subsection{Data Analysis \& Interpretation}

We randomly select one noise signal to analyse first. It has a mean voltage of $\mu(V) = 0.663mV$ that is close to zero, a variance of $Var(V) = 144mV^2$, and a standard deviation of $\sigma_V = 12.0mV$.

We plot a histogram of the samples by voltage in Fig \ref{fig:hist}. The plot can be well fitted by a Gaussian with the same mean and standard deviation as the samples. The width is around 3 times of the standard deviation.

\begin{Figure}
\centering
\includegraphics[width=\linewidth]{noise_hist.png}
\captionof{figure}{\label{fig:hist} Histogram of noise samples by voltage}
\end{Figure}

We plot the averages of $N = 1, 2, 4, 8, 16, 32$ power spectra of blocks of noise in Fig \ref{fig:noise2-16}. The different numbers of spectra averaged is equivalent to Fourier transforming the signal on a longer range of time.


\begin{equation}
t_{Integration} \propto N
\end{equation}
\begin{Figure}
\centering
\includegraphics[width=\linewidth]{noise2-16.png}
\captionof{figure}{\label{fig:noise2-16} Averages of power spectra of $N = 1, 2, 4, 8, 16, 32$ noise samples}
\end{Figure}

We first compare the single power spectrum with the average over $N = 32$. The shapes of the two spectra are similar, but the averaged one has a better-defined, flatter shape while that for one block is fuzzy and fluctuates wildly. Both have a primary peak at $\nu = 0$, suggesting a non-zero mean, and four side peaks between $15MHz$ and $30MHz$ with similar relative amplitude, but the ones for the single block are much higher relative to the central peak probably due to random fluctuations. The side peaks may be from the filter or  the noise generation mechanism.

To better explore the trends in between the two cases, we examine the other cases. Those spectra stand between the cases of $N = 1$ and $N = 32$. In particular, a higher $N$ makes the plot shape better defined and lowers the strength of the four side peaks relative to the central peak, while their overall shape and heights relative to each other are preserved.

If we regard the smooth distribution for $N \ge 32$ as a signal and the scatter around that curve as noise, we can claim that the signal-to-noise ratio (SNR) is decreased as a larger $N$ gets the power spectrum closer to the signal curve with less fluctuations. A more numerical analysis of the curves may yield the SNR's dependency relation on $N$.

To confirm the correlation theorem on autocorrelation functions (ACFs) again, we calculate the ACF of one block and plot it in Fig \ref{fig:noise_ACF}. Fig \ref{fig:noise_ACF_Zoomed} is a zoomed in version.

\begin{Figure}
\centering
\includegraphics[width=\linewidth]{noise_ACF.png}
\captionof{figure}{\label{fig:noise_ACF} Result of the autocorrelation function on a block of noise}
\end{Figure}

\begin{Figure}
\centering
\includegraphics[width=\linewidth]{noise_ACF_Zoomed.png}
\captionof{figure}{\label{fig:noise_ACF_Zoomed} Magnified result of the autocorrelation function on a block of noise}
\end{Figure}

The ACF on the noise block only has a narrow peak at the centre, in contrast to the overall hexagonal shape of a sine wave's ACF. The zoomed in wave-form looks like the original, but has a much higher amplitude by a factor of about $30000$.

We then Fourier transform the ACF and plot it in Fig \ref{fig:noise_ACF_FT}. The shape of the spectrum of the ACF looks highly similar to the power spectrum of the original signal, but the overall amplitude is larger by 3 orders of magnitude. That means the correlation theorem is mostly true, at least for ACFs, except that a normalisation factor should be added.

\begin{Figure}
\centering
\includegraphics[width=\linewidth]{noise_ACF_FT.png}
\captionof{figure}{\label{fig:noise_ACF_FT} Fourier transform of the autocorrelation function on a block of noise}
\end{Figure}

If we ignore the central peaks at $\nu = 0$ and consider the maxima of the two spectra to be the maxima of the primary side peaks, then the Full-Width Half-Max's would be around $FWHM = 18MHz$ for both the Fourier transform of the ACF and for the original power spectrum.

In conclusion, the correlation theorem stands for noise as it does for sine waves, especially in the case of autocorrelation functions, whose Fourier transforms can produce power spectra of the original signal.

\section{Sampling Multiple Sine Waves}

Signals can often be comprised of more than one sinusoidal wave.
The frequency resolution is the smallest resolvable frequency difference between two different signals. The smaller the resolution, the higher the precision a measurement of multiple combined signals can be. \cite{lab1}
We explore relevant consequences with the simple case of combining 2 sinusoidal signals.

\subsection{Methods}
We use two different signal generators: the Keysight N9310A used before and a Stanford Research Systems Model DS345, as we do not have multiple copies of any generator model. We combine their signals with a power splitter run backwards, feed them to the same Rigol DS1052E digital oscilloscope, and then our computers.

To find the frequency resolution of our setup, we generate two waves with similar frequencies $\nu_0 = 10MHz$ and $\nu = 10MHz + \Delta\nu$.

\subsection{Data Analysis \& Interpretation}

We guess the frequency resolution would be on a similar order of magnitude to frequency span of each bin in the Fourier transform, i.e. the sampling frequency divided by number of samples.


\begin{equation}
d\nu_{resolvable} \simeq \frac{\nu_s}{N}
\sim \frac{62.5MHz}{16000} \sim 3.9kHz
\end{equation}


We have taken many signals of $\Delta\nu \approx d\nu$, and Fig \ref{fig:resolutionFull} shows some power spectra of special interest.

\begin{Figure}
\centering
\includegraphics[width=\linewidth]{resolutionFull.png}
\captionof{figure}{\label{fig:resolutionFull} Power spectra of some dual-tone signals with small frequency differences}
\end{Figure}

The spectra all look sharp and similar, and the peaks are narrow. We zoom in onto them as Fig \ref{fig:resolution}:

\begin{Figure}
\centering
\includegraphics[width=\linewidth]{resolution.png}
\captionof{figure}{\label{fig:resolution} Zoomed in \& normalised power spectra of some dual-tone signals with small frequency differences}
\end{Figure}

We have also normalised the peaks for this figure to make features clearer.
At $\Delta\nu = d\nu = 3.9kHz$, there is no discernible double peak at all. The actual frequency resolution seems to be around $5.9kHz \approx 1.5 d\nu$, as the peaks at $\Delta\nu = 5.9kHz$ are clearly resolvable as those at $\Delta\nu = 5.8kHz$ look vague and more like a plateau than two peaks.
The difference from theoretical prediction is minor, and is probably due to the discrete nature of our Fourier transform as the peaks it obtains have finite width and a triangular shape instead of an ideal, infinitely narrow delta function.

\section{The Double-Sideband (DSB) Mixer}

A Double-Sideband (DSB) mixer multiplies two incoming signals, the usually fixed Local Oscillator (LO) and the variable Radio Frequency (RF), into one, the Intermediate Frequency (IF). It is useful in converting an RF signal to the LO's frequency range, which is often selected to be convenient for the equipment to sample and process. \cite{lab1}

In this section, we explore the properties of the DSB mixer and its IF outputs.

\subsection{Methods}

We use a single MiniCircuits ZAD-1 mixer to work as our DSB mixer. We use the same signal generators from before: the Keysight N9310A to produce the LO because it has been better calibrated and the Stanford Research Systems Model DS345 to make the RF. As in the last section, we plug the output IF signal from the mixer into the same Rigol DS1052E digital oscilloscope, and then our computers.

We choose the LO frequency to be $\nu_{LO} = 10.0MHz$ that is in the middle of the input range of the ZAD-1 and much lower than the Nyquist criterion for a plethora of samples per period, so that it would be mixed ideally and sampled clearly. We set the RF frequencies to be $\nu_{RF} = \nu_{LO} \pm \delta \nu$ where $\delta\nu = 5\% \nu_{LO} = 0.5MHz$ to make the difference from the LO frequency apparent. We take $N = 16000$ samples as usual.

We plot the wave-form of the output IF signal with $\nu_{RF} = 10.5MHz$ in Fig \ref{fig:dsb_wave-form}.

\begin{Figure}
\centering
\includegraphics[width = \linewidth]{dsb_waveform.png}
\captionof{figure}{\label{fig:dsb_wave-form} An IF signal from a DSB mixer, mixing $\nu_{LO} = 10.0MHz$ and $\nu_{RF} = 10.5MHz$}
\end{Figure}

It looks like a multi-tone signal with a high-frequency wave enveloped by a middle-frequency one that is in turn enveloped by another wave with slightly lower frequency. That is different from the oscilloscope trace, which looks like a sine wave constantly phase-shifting back and forth, like in  Fig \ref{fig:dsb_oscilloscope}, possibly because one of the major frequencies in the wave evenly divides the sampling rate of the oscilloscope.

\begin{Figure}
\centering
\includegraphics[width=\linewidth]{dsb_oscilloscope.jpg}
\captionof{figure}{\label{fig:dsb_oscilloscope} Oscilloscope trace of an IF signal from a DSB mixer, mixing $\nu_{LO} = 10.0MHz$ and $\nu_{RF} = 10.5MHz$}
\end{Figure}

\subsection{Data Analysis \& Interpretation}

Parsons has shown with some complex analyses that mixing two real sine waves with angular frequencies $\omega_0$ and $\omega_0 + \Delta\omega$ would yield two cosine waves, the Lower Sideband (LSB) with angular frequency $\omega_{beat} = \Delta\omega$ and the Upper Sideband (USB) with $2\omega_0 + \Delta\omega$. As the angular frequency $\omega$ is proportional to frequency $\nu$, the frequencies of the IF output are $2\nu_p + \Delta\omega$ and $\Delta\omega$.
However, if the RF is more complex and contains both of $\nu_0 \pm \Delta\nu$, the mixer would yield four cosine waves of frequencies $\pm \Delta\nu$ and $2\nu_0 \pm \Delta\nu$. As the cosine function is even, the $\pm \Delta\nu$ frequencies would be indistinguishable and stack together as the 'Double-Sideband' on the power spectrum. That is one inconvenience of the DSB mixer. \cite{lab1}

Theoretically, if $\nu_{RF} = \nu_{LO} \pm \delta \nu = (10.0 \pm 0.5)MHz$, then the output IF should have two frequencies, $\nu_{beat} = \pm\Delta\nu = \pm0.5MHz$ and $\nu_{USB} = (20.0 \pm 0.5)MHz$. The $16000$ samples should enough to give us several periods of the slowest wave at the Lower Sideband.

However, there also exist intermodulation products, which are harmonics of the inputs, the sum, and harmonics of  harmonics produced by non-linear diodes in real mixers. The diodes only perform an approximate multiplication of the input signals. \cite{lab1}

We plot the power spectra of $\nu_{RF} = \nu_{LO} \pm \delta \nu$ mixed with $\nu_{LO}$ in Fig\ref{fig:dsb} to confirm. We do see the sidebands where they should be: the Lower Sidebands at $\nu_{beat} = 0.5MHz$ with lower frequency than $\nu_0$ for both cases, and the Upper Sidebands at $\nu_{USB} = 19.5MHz$ for $\nu_{RF} = 9.5MHz$ and $\nu_{USB} = 20.5MHz$ for $\nu_{RF} = 10.5MHz$. However, there are also minor peaks about $0.5MHz$ away from the Upper Sidebands, which may be intermodulation products. More precise numerical analyses should be done to confirm those statements.

\begin{Figure}
\centering
\includegraphics[width=\linewidth]{dsb.png}
\captionof{figure}{\label{fig:dsb} Power spectra of IF signals from a DSB mixer}
\end{Figure}

To examine intermodulation products more closely, we plot the power spectrum of the wave with $\nu_{RF} = 10.5MHz$ in log scale in Fig\ref{fig:dsb_log}. There indeed are pronounced intermodulation products, many of which seem $0.5MHz$ apart. A cluster near the second harmonic of the Upper Sideband, $\nu = \frac{\nu_{USB}}{2} = \frac{20.5MHz}{2} = 10.3MHz$, is particularly prominent.

\begin{Figure}
\centering
\includegraphics[width=\linewidth]{dsb_log.png}
\captionof{figure}{\label{fig:dsb_log} Power spectrum of the IF signal with $\nu_{RF} = 10.5MHz$ from a DSB mixer}
\end{Figure}

Fourier filtering is the zeroing the height of a certain frequency bin in a spectrum from a Fourier transform. It can clean unwanted portions of a signal.
To recover a cleaner signal at beat frequency, we Fourier filter the signal with $\nu_{RF} = 10.5MHz$ by zeroing the Upper Sideband and recreate a signal by taking the inverse transform of the filtered spectrum. The signal is plotted in Fig \ref{fig:dsb_filtered}.

\begin{Figure}
\centering
\includegraphics[width=\linewidth]{dsb_filtered.png}
\captionof{figure}{\label{fig:dsb_filtered} An IF signal from a DSB mixer, with the Upper Sideband $\nu_{USB} = 20.5MHz$ Fourier filtered}
\end{Figure}

The wave-form is much narrower than the original in Fig \ref{fig:dsb_wave-form}, with a major envelope removed. However, several others remain, so the wave still looks like a triple-tone signal. Therefore, a low-pass filter allowing only the part of the signal near the Lower Sideband may be better than Fourier filtering at cleaning a mixed signal.

\section{The Single-Sideband (SSB) Mixer}

Single-Sideband (SSB) Mixers are made to sidestep the flaw in DSB mixers that conflate information on Lower Sidebands with different signs. \cite{lab1} In this section, we explore their properties.

\subsection{Methods}

We construct an SSB mixer following the schematic Fig 2. in Parsons In particular, we split the RF signals from the Keysight N9310A generator used before with A T-joint and connect each to a ZFM-15 mixer. We use the Stanford Research Systems Model DS345 as the LO and feed it to both mixers, but one of the routes is a direct connection of metal ports while the other is a 12-feet one that should create a $\frac{\pi}{2}$ phase delay. The output from mixer R with no delay is plugged into Channel 1 of the oscilloscope and then the PicoSampler ADC and is taken as the real part of the IF. The output from mixer I connected to the delay cable is connected to Channel 2 of the oscilloscope and then the ADC and is taken as the imaginary part.

Fig \ref{fig:ssb} depicts the SSB mixer, but with the delay cable replaced by a short cable to emulate a DSB mixer where the same LO signal goes to both ZFMs. We use 2 T-joints for one ZFM and no T-joint for the other to balance the RF and LO strength and phase for each ZFM.

\begin{Figure}
\centering
\includegraphics[width=\linewidth]{ssb.jpg}
\captionof{figure}{\label{fig:ssb} Our SSB mixer construct, with the delay cable replaced by a short cable to emulate a DSB mixer}
\end{Figure}

We pick the LO frequency to be $13.4500MHz$ so that the 12-feet cable would create the desired phase delay of $\frac{\pi}{2}$. RF frequencies to be $\nu_{RF} = \nu_{LO} \pm \delta \nu = (13.4500 \pm 0.6725)MHz$. Again, we take $N = 16000$ samples per channel.

We first use the DSB-emulating setup in Fib \ref{fig:ssb} and plot the power spectra for $\nu_{RF} = (13.4500 \pm 0.6725)MHz$ in Fib \ref{fig:ssb_dsb}. Then we repeat the process in true SSB mode with the delay cable in place. We plot the power spectra of the resulting IF waves in Fig\ref{fig:ssb_ssb}.

\begin{Figure}
\centering
\includegraphics[width=\linewidth]{ssb_dsb.png}
\captionof{figure}{\label{fig:ssb_dsb} Power spectra of IF outputs of our SSB mixer emulating a DSB mixer}
\end{Figure}

\begin{Figure}
\centering
\includegraphics[width=\linewidth]{ssb_ssb.png}
\captionof{figure}{\label{fig:ssb_ssb} Power spectra of IF outputs of our SSB mixer in SSB mode}
\end{Figure}

\subsection{Data Analysis \& Interpretation}

As proved in Parsons, an SSB mixer with a real sine wave with two frequencies $\nu_{RF} = \nu_0 \pm \Delta\nu$ as the RF input would output four complex sine waves of frequencies $\nu_{beat} = \pm \Delta\nu$ and $2\nu_0 \pm \Delta\nu$. In contrast to the IF output of DSB mixers, the SSB IF would be uneven complex sine waves and therefore could distinguish between the positive and negative sidebands, giving different heights and thus, differing information on the two. Again, the sidebands corresponding to $\pm \Delta\nu$ are the Lower Sidebands and those corresponding to  \cite{lab1}

Power spectra in the DSB-emulating mode in \ref{fig:ssb_dsb} hints at that properties property by being antisymmetric about the y-axis, which disagrees with the behaviour of an actual DSB mixer. That is because the connections inevitably delays LO signals through the two routes unevenly, making it impossible for the setup to perfectly emulate a DSB mixer.
Apart from the imperfection, the IF signal does include both the lower and Upper Sidebands at the correct frequencies $\nu_{beat} = \pm 0.6725MHz$ and $\nu_{USB} = (26.9000 \pm 0.6725)MHz$. It has minor intermodulation products as before, too.

Power spectra in true SSB mode in \ref{fig:ssb_ssb} has its asymmetry much more pronounced. For $\nu_{beat} = -\Delta\nu$, the positive Lower Sideband is almost zero, as expected. For $\nu_{beat} = +\Delta\nu$, the negative Lower Sideband is almost zero. Nevertheless, neither Lower Sidebands are actually zero, suggesting that the strength balance and $\frac{\pi}{2}$ phase delay of the LO signal are imperfect and could not cancel terms out as dictated by theory.
Upper Sidebands for both signals are very asymmetric, too, with the positive one much larger than the negative one.
In conclusion, an SSB mixer can hardly emulate a DSB one, but can produce IF signals preserving different information for frequencies with different signs as desired, though perfection may require tuning of the setup.
\section{Conclusion}
We have used experiments to explore the results of Fourier transforms and Fourier filtering, to confirm the Nyquist criterion for sampling single-tone sine waves and the correlation theorem in the case of autocorrelation functions, and to observe power leakage, the effect of wide Nyquist windows, the properties of generated noise and the effect of longer integration time on it, and the practical advantages and limitations of Double-Sideband and Single-Sideband mixers.
Some aspects of the project are lacking: some samples and their numerical analyses are not enough for strong conclusions, as we focus too much on estimating plots with the human eye. Normalising signals may also improve the precision of some results. Meanwhile, the design of our circuits, most notably the Single-Sideband mixer, is not always best-considered and contain asymmetries that may introduce systematic offsets in the output. We could improve that with a more meticulous design.
In the future, we hope to improve our work in the said ways, and use the knowledge and tool gained from this project to aid the radio astronomy community in observations and signal processing.

\bibliographystyle{plain}
\begin{thebibliography}{}

  \bibitem[Parsons, 2020]{lab1} Parsons, Aaron. 2020,Lab 1: Exploring Digital Sampling, Fourier Filtering, and Heterodyne Mixers

  \end{thebibliography}{}
  \end{multicols}
\end{document}
