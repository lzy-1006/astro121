\documentclass[12pt, letterpaper]{article}

%% Language and font enco
\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{siunitx}

%% Sets page size and margins
\usepackage[letterpaper,top=2.54cm,bottom=2.54cm,left=2.54cm,right=2.54cm,marginparwidth=1.75cm]{geometry}

%% Useful packages
\usepackage{multicol,caption}
\usepackage{graphicx}
\newenvironment{Figure}
  {\par\medskip\noindent\minipage{\linewidth}}
  {\endminipage\par\medskip}

% \usepackage[
% %   subtle
%   moderate
% ]{savetrees}

\setlength {\marginparwidth}{2cm}

\title{An HI Survey across Galactic Longitudes $\ang{130}$ \& $\ang{310}$}
\author{Ziyi Lu, of Team Rocket}

\begin{document}
\maketitle

\setlength{\abovedisplayskip}{10pt}
\setlength{\belowdisplayskip}{10pt}

\begin{abstract}
We use the dish at Leuschner Observatory to survey the HI frequency range on galactic longitudes $\ang{130}$ and $\ang{310}$, which connect to form a circle. We have roughly mapped out the velocity and column density distribution along them, identifying a main feature mostly uniform in position and frequency, brightest near the galactic plane but also visible at other discrete galactic latitudes. A distinct feature is brighter than that and looks like a narrow high-energy object receding rapidly near the known position of the receding star cluster NGC 663, but we are unconfident in any connection. There is an overall velocity trend maximised near $(\ang{130}, \ang{50})$ and lower elsewhere with many fluctuations, possibly due to an overall motion of galactic HI. Column density $N$ is mostly non-zero except for an asymmetric peak near the galactic plane. We propose many potential improvements, most notably the mitigation of saturation by and compression of high-power signals from the system. \end{abstract}

\begin{multicols}{2}

\section{Introduction}
Atomic hydrogen, i.e. HI, is a form of hydrogen gas at low energy and therefore not excited enough to separate into atoms or even ions but also having enough energy from ultraviolet photons to not bind into molecules. Diffuse HI occupies large fractions of interstellar space in the Milky-way Galaxy. \cite{lab4}

HI characteristically emits the 21-centimetre line, also known as the HI line, as the alignment of the spins of an HI atom's sole electron and that of its nucleus change from the same to the opposite. Its wavelength is $\lambda = 21\si{\centi\metre}$ when emitted, and the frequency is $\nu = 1.4204\si{\giga\hertz}$. As it is a hyperfine line precisely emitted only from HI due to this quantum phenomenon, it is an accurate messenger of information from HI regions, e.g. the column density of HI in them. \cite{lab2}

We seek to learn the thickness and kinematics of HI gas, characterised by column density and velocity. Column density $N$ is the mass area density $\rho_A$ integrated along a path $s$, which is usually the line of sight from infinity to the observer in observational astronomy, including in our case. It can be calculated from the radiative transfer equation that
\begin{equation}
    N = 1.82 \times 10^{18} \int T_B(v) dv \si{\centi\metre}^{-2}
\label{eq:col_density} \end{equation}
\noindent where $v$ is the velocity $v = -c \frac{\Delta\nu}{\nu}$ and $T_B(v)$ is the brightness temperature of the gas at that velocity. $T_B = T_A$, the antenna temperature, if the source spans the whole beam of the antenna, like in our case where we map the sky for no specific target. \cite{lab4}

We aim to map HI distribution within our Milky-way Galaxy across galactic longitudes $\ang{130}$ and $\ang{310}$, which connect at galactic coordinates $(l, b) = (\ang{130}, \pm\ang{90})$ to form a great circle, as $\ang{310} = \ang{130} + \ang{180}$. To ease continuous representation, we define Effective Galactic Latitude $b_{eff}$ so that
\begin{equation}
    b_{eff} \equiv \{
    \begin{array}{cl}
        b & \text{for } l \in [0, \ang{180}) \\
        180 - b & \text{for } l \in [\ang{180}, \ang{360}) \; \& \; b \geq 0 \\
        -180 - b & \text{for } l \in [\ang{180}, \ang{360}) \;\&\; b < 0
    \end{array}
\end{equation}
\noindent Thus, the $b_{eff}$ for galactic longitude $\ang{130}$ would be the same as $b$. Meanwhile, on galactic longitude $\ang{310}$, $b = \ang{88}$ would map to $b_{eff} = \ang{92}$ next to $(\ang{130}, \ang{90})$ and $b = \ang{-88}$ would map to $b_{eff} = \ang{-92}$ next to $(\ang{130}, \ang{-90})$ like on the celestial sphere.

The Milky-way has most optically visible matter concentrated near the galactic plane, i.e. $b = \ang{0}$, so we expect most HI to be concentrated there due to gravitation. It is also known that there is a baryonic halo at all galactic latitudes, with globular clusters and clouds dispersed in it, so we anticipate existence of HI at other latitudes with some compact HI regions, too. Additionally, we specifically look for high-velocity gas at positive latitudes at $l = \ang{130}$ or $\ang{310}$ as reported by previous studies. \cite{lab4}

We employ frequency-switching to get rid of frequency-dependent systematics in our spectra by changing the Local Oscillator (LO) frequency $\nu_{LO}$ in our mixer. The mixer multiplies two incoming sinusoidal signals to output complex sine waves, the useful ones of which are at difference frequencies of the inputs. The rest are at sum frequencies and are low-pass filtered.

By receiving the same signal with different $\nu_{LO}$, we shift the position of the signal in our spectra with the same filter systematics in the same place. The difference of two spectra of the same target with different $\nu_{LO}$ is therefore solely two copies of the signal with the systematics eliminated. \cite{lab1} We will elaborate on the specifics of this operation in our project in Section 2.

We determine the integration time $\tau$ of our observations based on the desired Signal-to-Noise Ratio, or SNR, a measure of the clarity of a signal compared to unwanted noise. According to the Radiometer Equation, a corollary of the CLT, it is
\begin{equation}
    \mathrm{SNR} \equiv \frac{T}{T_{sys, rms}} = \frac{T}{T_{sys}}\sqrt{B\tau}
\end{equation}
\noindent where $T$ is the brightness temperature of the signal and $T_{sys}$ is that of the systematics, and $B$ is the bandwidth integrated bandwidth over which the observation is made. For a target SNR,
\begin{equation}
    \tau \geq \frac{(\mathrm{SNR}\frac{T_{sys}}{T})^2}{B}
\label{eq:radiometer} \end{equation}

Temperature estimations require the Central Limit Theorem (CLT). It is helpful in states that a sample of means or sums of any large random population should follow a normal distribution. In practice, it means that random processes like the thermal process would produce bell curves for random variables like velocity.

For example, random thermal movement of charge carriers in a circuit will have their velocities along the wires follow a normal distribution. As current $I \propto qv$ where $q$ is the charge of the charge carrier and $v$ is its velocity along the wires, the distribution of values of the current will be a bell curve in the long term. If resistance $R$ is constant for the same circuit, then voltage $V = IR$ will also follow a normal distribution in time. Similarly, particles in a hot gas cloud will have their radial velocities follow a normal distribution, and the resultant Doppler shift will make a fine spectral line become a wide Gaussian. In that case, we may estimate the temperature from the width of the Gaussian. A wider curves implies a hotter target. \cite{lab2}

Nevertheless, Gaussian thermal broadening is only one of the reasons for the width of an HI line. The overall broadening can often be characterised by the Voigt profile, a convolution of a Gaussian with intrinsic Lorentzian broadening due to the uncertainty principle. However, the Lorentzian is most prominent at the far sides while the broadening at the centre of a peak is dominated by the Gaussian. As we anticipate our instrument to be only sensitive enough for the centres of peaks, we expect the main contribution for visible broadening to be thermal. For the same reason, we also ignore minor sources for line broadening, e.g. collisional broadening.

In section 2, we describe the hardware setup of our observing system; in section 3, we reduce and analyse the spectral data; in section 4, we physically interpret the data as observed objects in the sky; in section 5, we discuss out shortcomings and potential improvements.

\section{Methods}

We use Leuschner Observatory (LEO)'s $4.5\si{\metre}$-diameter dish to map out HI at galactic longitudes $\ang{130}$ \& $\ang{310}$ from JD $2458962.80$ to $2458963.27$ on 2020-4-23 PST. It is at latitude $\lambda = \ang{37.9}$ North, longitude $\phi = \ang{122.1}$ West, Lafayette, California, the United States. Its angular resolution $d\theta$ is
\begin{equation}
d\theta = \frac{d}{\lambda} = \frac{21.1\si{\centi\metre}}{4.5\si{\metre}} = 0.047 rad = \ang{2.7}
\end{equation}
\noindent where $d$ is the diameter and $\lambda$ is the wavelength, so we choose our pointings to be $\ang{2}$ apart, though that is lower than the Nyquist sampling frequency $d\theta_{Nyquist} = \frac{d\theta}{2} = \ang{1.3}$, as we do not expect observing fine structure.

The dish can only point to positions with altitude $\mathrm{Alt} \in [14, 85]$ and azimuth $\mathrm{Az} \in [5, 350]$, and cannot point to the north. Targets that have declination $\delta \geq \ang{90} - \ang{5} = \ang{85}$ will always be too northern and those with declination $\delta \leq \ang{14} - (\ang{90} - \mathrm{latitude}) = -\ang{38.1}$, so they will never be visible. Among our pointings, those at galactic longitude $l = \ang{310}$ and galactic latitude $b \in []\ang{-78}, \ang{24}]$ are unobservable for this reason, so we only point at the targets with $b \in []\ang{-88}, \ang{-80}] \cup [\ang{26}, \ang{88}]$ among the ones with $l = \ang{310}$, totalling $128$ pointings.

To discard signals outside the HI line range, the signal is band-pass filtered to a range of $1.40 - 1.63\si{\giga\hertz}$. It is then down-mixed with a configurable Local Oscillator (LO) to compensate for low-frequency instruments. We set the LO frequencies to be $\nu_{LO0} = 1270\si{\mega\hertz}$, $\nu_{LO1} = 1269\si{\mega\hertz}$ so that the signal is down-converted to a range of $130 - 360 \si{\mega\hertz}$ for LO0 and $131 - 361 \si{\mega\hertz}$ for LO1. We again bandpass-filter the Intermediate Frequency (IF) output to a more relevant range of $145 - 155\si{\mega\hertz}$ and sampled by a spectrometer to output a power spectrum.

The spectrometer is an FPGA ROACH (Reconfigurable Open Architecture Computing Hardware) from the CASPER project (Collaboration for Astronomy Signal Processing \& Electronics Research). It samples at $768\si{\mega\hertz}$, which is unnecessarily high, so we drop $\frac{31}{32}$ of the samples to down sample to $24\si{\mega\hertz}$, resulting in a spectrum of $\nu \in [-12, 12]\si{\mega\hertz}$. The signal is real, so the spectra are repetitively symmetric about the power axis. Therefore, we only record the positive half, resulting in a bandwidth of $B_{specrometer} = 12\si{\mega\hertz}$. The power axis is in great numerical precision, so we ignore the error for that.

As there are $8192$ frequency channels per spectrum, the frequency resolution is $\pm d\nu = \frac{12\si{\mega\hertz}}{8192} = \pm0.001\si{\mega\hertz}$. The final spectra are aliased from $144 - 156 \si{\mega\hertz}$ of the down-mixed signals at the even 12th Nyquist window and is not inverted by the frequency axis. The spectrometer outputs spectra in two orthogonal polarisations each time, which we call polarisations 0 and 1. The bandpass filter should make the spectra only non-zero at $1 - 11\si{\mega\hertz}$. We expect the HI line to be near $6.4\si{\mega\hertz}$ for $\nu_{LO0} = 1270\si{\mega\hertz}$ and near $7.4\si{\mega\hertz}$ for $\nu_{LO1} = 1269\si{\mega\hertz}$.

To convert power values $P$ from the spectrometer to brightness temperature $T_B$, we inject white noise through a noise diode with known brightness $80\si{\kelvin}$ in polarisation 0 and $270\si{\kelvin}$ in polarisation 1. It will produce a voltage response in the spectra that can be used to calculate gain $g = \frac{T_b_{noise}}{P_{noise}}$.

The default integration time of each spectrum is $\tau_0 = 0.699\si{\second}$, and we must take multiple spectra for longer time. While we find it unnecessary to take more than one spectrum for the noise per pointing, we calculate the number of signal spectra needed per pointing to achieve an SNR of $10$ with Eq \eqref{eq:radiometer}, the Radiometer Equation. As the final bandpass filter only allows a bandwidth of $B = 10\si{\mega\hertz}$ and we anticipate to observe targets at $1\si{\kelvin}}$ at the darkest,
\begin{equation} \begin{split}
    \tau \geq \frac{(\mathrm{SNR}\frac{T_{sys}}{T})^2}{B} \\
    = \frac{(10 \frac{600\si{\kelvin}}{1\si{\kelvin}})^2}{12\si{\mega\hertz}} \\
    = 3\si{\second}
\end{split} \end{equation}
\noindent As this is an order-of-magnitude estimate, we take $10$ spectra summed up to $6.99\si{\second}$ for each $\nu_{LO}$ per pointing to be safe.

We move the dish to get the most precise pointing possible after each integration.

We end up taking $508$ data files for $127$ pointings, with 4 files per pointing for two $\nu_LO$ and with the noise off or on. Each file has spectra for two polarisations. We miss $(\ang{310}, \ang{-88})$ due to a combination of programming carelessness and a floating point error.

We respectively average all files with more than one spectrum to two median spectrum per file, one per polarisation, to utilise the longer effective integration time of taking multiple spectra under the same conditions. We then use a moving mean over $100$ channels to smoothen all $1016$ resultant spectra. The number of channels become $8093$ per spectrum after smoothening, keeping the frequency resolution around $\pm d\nu = \frac{12\si{\mega\hertz}}{8192} = \pm 0.001\si{\mega\hertz}$.

To get an idea of our data, we plot four spectral maps for polarisation 0 in Fig \ref{fig:raw}, at different $\nu_{LO}$s and $b_eff$s without and with noise. Those for polarisation 1 look almost identical. The colour-bar is in an arbitrary unit of power. There is a prominent latitude-dependent line near $6.4\si{\mega\hertz}$ for $\nu_{LO0} = 1270\si{\mega\hertz}$ and near $7.4\si{\mega\hertz}$ for $\nu_{LO1} = 1269\si{\mega\hertz}$ which is almost zero at certain latitudes, as expected for the HI line. We thereafter assume that line is not systematics or any other distraction, but is the HI line. We will try to isolate it from the systematics that span all frequencies in the next Section.

\begin{Figure}
    \centering
    \includegraphics[width=\linewidth]{raw}
    \captionof{figure}{\label{fig:raw} Spectral maps for polarisation 0 in Fig \ref{fig:raw}, at different $\nu_{LO}$s and $b_eff$s without and with noise. The missing $(\ang{310}, \ang{-88})$ pointing shows up as a dark horizontal line at $b_{eff} = \ang{-92}$. The bandpass filter at $1 - 11 \si{\mega\hertz}$ to be imperfect, esp. at $0 - 1 \si{\mega\hertz}$. There is a prominent latitude-dependent line near $6.4\si{\mega\hertz}$ for $\nu_{LO0} = 1270\si{\mega\hertz}$ and near $7.4\si{\mega\hertz}$ for $\nu_{LO1} = 1269\si{\mega\hertz}$ which is almost zero at certain latitudes, as expected for the HI line. The systematics that span all frequencies are in stripes because we observed slices of contiguous pointings at different time-blocks. Many of them have spatially-dependent frequency-periodic ripples probably from standing waves formed between our dish and nearby fences. The data with noise have similar features to the ones without it, but are brighter and murkier.}.
\end{Figure}

\section{Data Analysis}

To isolate the HI signal from the data without noise, we rid of the systematics that span all frequencies, esp. the spatially-dependent frequency-periodic ripples. We do that by least-square-fitting curves to the part of the spectra we take to be the systematic baseline, roughly within the bandpass filter but ignoring the HI range around $6$ or $7\si{\mega\hertz}$.

For pointings with no visible ripple, we fit a polynomial with order $5$ to $9$ as ad hoc determined for different stripes through trial and error. For those with ripples, we fit the addition of a polynomial and a sine wave.  E.g., we plot the expected HI line position. the original data, the fitted baseline, and their difference (i.e. the supposed HI signal) for polarisation 0 at $\nu_{LO0} = 1270\si{\mega\hertz}$ for $(\ang{130}, \ang{0})$ (without ripples) and $(\ang{130}, \ang{150})$ (with ripples) in Fig \ref{fig:baseline} to demonstrate.

\begin{Figure}
    \centering
    \includegraphics[width=\linewidth]{baseline}
    \captionof{figure}{\label{fig:baseline} The expected HI line position, original data, the fitted baseline, and their difference (i.e. the supposed HI signal) for polarisation 0 at $\nu_{LO0} = 1270\si{\mega\hertz}$ for $(\ang{130}, \ang{0})$ (Upper Panel) and $(\ang{130}, \ang{150})$ (Lower Panel). The baselines are non-zero but low outside the bandpass filter $1 - 11 \si{\mega\hertz}$. The peaks near the expected HI line are close enough to the expected frequency. The one in the lower panel is not as prominent as the upper one, but it disrupts the frequency-periodicity of the curve and is therefore probably not part of the ripple, but a signal. The difference is mostly the peak only, but is not perfectly flat at its sides, with false spikes for the lower panel.}
\end{Figure}

The peaks near the expected HI line are close enough to the expected frequency. The one in the lower panel is not as prominent as the upper one, but it disrupts the frequency-periodicity of the curve and is therefore probably not part of the ripple. Thus, they match our expectations of the HI line signal.

We then calibrate the y-axis by measuring instrumental response to the injected white noise of known brightness temperature. We examine the shapes of the difference of the fitted baselines with and without noise and examine them for both polarisations at $\nu_{LO0} = 1270\si{\mega\hertz}$ for $(\ang{130}, \ang{150})$ without ripples in Fig \ref{fig:noise} and at $(\ang{130}, \ang{150})$ with ripples in Fig \ref{fig:noise_rippled}.

\begin{Figure}
    \centering
    \includegraphics[width=\linewidth]{noise}
    \captionof{figure}{\label{fig:noise} The differences of the fitted baselines with and without noise for polarisations 0 (Upper Panel) and 1 (Lower Panel) at $(\ang{130}, \ang{0})$ and $\nu_{LO0} = 1270\si{\mega\hertz}$.
    For polarisation 0, the data with noise is almost at higher at frequencies with no peak, but almost the same at the peak, probably because the power there is strong enough to saturate our instrument. The difference seems to have a linear trend. As the noise is white noise with a flat spectrum, we consider that to be the result of a linear instrumental response to signals and fit a line to the difference in the $1 - 11 \si{\mega\hertz}$ bandpass filter range, excluding the HI line range.
    For polarisation 1, the data with noise gets below the ones without it at certain frequencies, probably due to an extreme case of the aforementioned saturation, in which a large slice of the spectrum is compressed, as the noise is much brighter in polarisation 1. We decide to investigate later and give up processing polarisation 1 data in the present project.}
\end{Figure}

\begin{Figure}
    \centering
    \includegraphics[width=\linewidth]{noise_rippled}
    \captionof{figure}{\label{fig:noise_rippled} The differences of the fitted baselines with and without noise for polarisations 0 (Upper Panel) and 1 (Lower Panel) at $(\ang{130}, \ang{0})$ and $\nu_{LO0} = 1270\si{\mega\hertz}$.
    For polarisation 0, the data with noise is almost at higher at frequencies with no peak, and the difference seems to have a linear trend with fluctuations for pointings with ripples like this one. We still fit a line to it because we consider the fluctuations a consequence of the ripples, therefore external instead of instrumental and negligible for calibration purposes.
    For polarisation 1, the data with noise gets below the ones without it at certain frequencies again. While the data for both polarisations are taken concurrently, those with no noise do not have ripples like all other in this figure. That could be because the ripples are from reflection and therefore are polarised. We plan to look into it after making polarisation 1 data usable with a difference (i.e. noise response) that is always positive.}
\end{Figure}

For polarisation 0, The difference always seems to have a linear trend, with fluctuations for pointings with ripples. As the noise is white noise with a flat spectrum, we consider that to be the result of a linear instrumental response to signals. We consider the fluctuations a consequence of the ripples, therefore external instead of instrumental and negligible for calibration purposes. We fit a line to the difference in the $1 - 11 \si{\mega\hertz}$ bandpass filter range, excluding the HI line range, and use that in our gain calculation, in which we equate anywhere on the line to have brightness temperature $T_{B, noise} = 80\si{\kelvin}$. The line's range is about $13\si{\kelvin}$ wide, so we take the error of the brightness temperature $dT_B$ to be $\pm 7\si{\kelvin}$ in case the non-zero slope is entirely from the error.

For polarisation 1, the data with noise gets below the ones without it at certain frequencies, probably due to an extreme case of the aforementioned saturation, in which a large slice of the spectrum is compressed, as the noise is much brighter in polarisation 1. While the data for both polarisations are taken concurrently, those with no noise do not have ripples like all other in this figure. That could be because the ripples are from reflection and therefore are polarised. We decide to investigate later and give up processing polarisation 1 data in the present project.

Finally, we reduced the frequency-switched data for both $\nu_{LO}$s. \cite{lab2} We cut the difference between $\nu_{LO0} = 1270\si{\mega\hertz}$ and $\nu_{LO1} = 1269\si{\mega\hertz}$ data between their HI signals at $\nu = 6.1\si{\mega\hertz}$, invert and translate the LO1 data by the difference between the $\nu_{LO}$s to make their HI lines overlap, and take the average of the overlapping part. We illustrate that last step for $(\ang{130}, \ang{0})$ in Fig \ref{fig:freq_switch}.

\begin{Figure}
    \centering
    \includegraphics[width=\linewidth]{freq_switch}
    \captionof{figure}{\label{fig:freq_switch} Inverted and translated data at different $\nu_LO$s, and their mean for $(\ang{130}, \ang{0})$. The mean is the final reduction of the frequency-switched data.}
\end{Figure}

We map the final spectra in Fig \ref{fig:map} with the color-bar indicating brightness temperature $T_B$ in units of $\si{\kelvin}$. We also plot evenly-spaced exemplary spectra in Fig \ref{fig:examples} as well as the position of the expected HI line. Most have only one peak and look very roughly Gaussian, though confirmation would require meticulous curve-fitting.

\begin{Figure}
    \centering
    \includegraphics[width=\linewidth]{map}
    \captionof{figure}{\label{fig:map} Final map of the reduced HI spectra. It is mostly dark except at certain $b_eff$s near the HI frequencies. It looks more spatially discrete than the raw data in Fig \ref{fig:raw} is, probably due to imperfect reduction. Most bright blobs are centred around $\nu \simeq 1420.44\si{\mega\hertz}$ and $0.2\si{\mega\hertz}$ wide, except for an additional feature on the galactic plane at $\nu \simeq 1420.65\si{\mega\hertz}$ that is narrower in height (spanning $\ang{18}$) but wide in frequency (spanning $0.5\si{\mega\hertz}$), indicating high energy. It is also the brightest feature at $T_B \simeq (104 \pm 7)\si{\kelvin}$ at maximum, suggesting a distinct narrow high-energy object receding rapidly, as it is probably the HI line at this frequency range and strength. It is near the receding star cluster NGC 663, but we are unconfident in any connection.
    Features near $\nu \simeq 1420.44\si{\mega\hertz}$ with similar widths are also the brightest near the galactic plane ($b_{eff} = \ang{0}$) at $T_B \simeq (90 \pm 7)\si{\kelvin}$ at the highest. The main blob there extends to $b = \pm\ang{24}$. There are also discrete-looking features at lower latitudes, all the way down to $b = -\ang{74}$. Besides, there is a cloud on $l = \ang{310}$, $b \in [\ang{30}, \ang{48}]$ around $T_B \simeq (23 \pm 7)\si{\kelvin}$. Their frequencies are slightly higher than HI emission frequency, suggesting recession.}
\end{Figure}

\begin{Figure}
    \centering
    \includegraphics[width=\linewidth]{examples}
    \captionof{figure}{\label{fig:examples} Evenly-spaced exemplary spectra compared to the expected HI line. The brightness temperature $T_B$ spans 2 orders of magnitude. All spectra have peaks with slightly higher frequency than the HI line, suggesting redshift. Most have only one peak and look very roughly Gaussian, though confirmation would require meticulous curve-fitting.}
\end{Figure}

\section{Interpretation}

We examine the map in further detail to look for potential physical features.

We try to find velocities of the observed gas, assuming what we see are indeed HI signals at this frequency range and strength. We identify the maxima in the spectra naively, calculate the Doppler velocities, correct for the spin and revolution of the Earth, proper movement of the Sun and the target, and relativity with the \verb$ugradio$ package \cite{lab2}, and plot them in \ref{fig:vel}. The error in velocity is $\pm \Delta v = 2 \frac{\pm\Delta \nu c}{\nu} = 2\frac{\pm 0.001 \times 3 \times 10^{5}}{1420.4}) = 0.2 \si{\kilo\metre\second^{-1}}$, doubled to account for imprecisions in corrections for Earth's motion. It is quite small, but it is also notable that positions of most naive maxima are darker than the brightness error margin and therefore untrustworthy.

\begin{Figure}
    \centering
    \includegraphics[width=\linewidth]{vel}
    \captionof{figure}{\label{fig:vel} Doppler velocity distribution of naive maxima of the spectra with terrestrial movement corrected. Vertical lines mark critical positions at $b = \ang{0}$ (on the galactic plane) and $\pm\ang{90}$ (at galactic zeniths). There is an overall trend maximised near $b = \ang{50}$ at $26\si{\kilo\metre\second^{-1}}$ and lower elsewhere with many fluctuations, possibly due to an overall motion of galactic HI. The minimum is at $\ang{-94}$ and $-35\si{\kilo\metre\second^{-1}}$. There are two gigantic negative deviances from the trend at $b_{eff} = \ang{0}$ and $\ang{57}$.}
\end{Figure}

There are two gigantic negative deviances from the trend at $b_{eff} = \ang{0}$ and $\ang{57}$. The former at $-42\si{\kilo\metre\second^{-1}}$ has been already been identifies in Fig \ref{fig:map}. We magnify the spectral map near the other at $-32\si{\kilo\metre\second^{-1}}$ in Fig \ref{fig:57} to examine it. It is seemingly from an approaching cloud, but its brightness is too much lower than the error margin for any conclusion.

\begin{Figure}
    \centering
    \includegraphics[width=\linewidth]{57}
    \captionof{figure}{\label{fig:57} Spectral map near the velocity anomaly at $b_{eff} = \ang{57}$. There is an extended noisy high frequency feature that is separate from the previously observed main trend near $\nu \simeq 1420.44\si{\mega\hertz}$ with maximum brightness temperature $T_B = (2 \pm 7)\si{\kelvin}$. It has similar width to that of the main trend, suggesting similar temperature. Its extended spatial profile and he fact that it does not disappear in spectra with the main trend in them are promising in suggesting that is an approaching physical cloud, rather than a systematic one. However, its brightness that is much lower than the error margin defies any conclusion.}
\end{Figure}

We then calculate the column density $N$ distribution with Eq \eqref{eq:col_density}. As our data are discrete, we replace the integral over frequency with a sum over channels in the observed HI range $\nu \in [1420.3\si{\mega\hertz}, 1421.0\si{\mega\hertz}]$ times the velocity of that channel, i.e.
\begin{equation}
    N = 1.82 \times 10^{18} \sum{b_{eff}} (T_B(v)\Delta v) \si{\centi\metre}^{-2}
\end{equation}

We plot the resultant column density distribution in \ref{fig:density}. We consider the error $\pm\Delta N$ that of brightness, i.e. about $\frac{7}{90} \simeq 8\%$. The curve has a peak an asymmetric peak near the galactic plane of $b_{eff} = \ang{0}$, reaching $\ang{-24}$ in the negative spatial direction and $\ang{35}$ in the positive one. It probably corresponds to the galactic disc, and the high-energy object identified in \ref{fig:map} is probably part of spike. Meanwhile, the curve is mostly near zero elsewhere, including at the velocity anomaly near $\ang{57}$ examined in \ref{fig:57}, so that is probably not a physical object.

\begin{Figure}
    \centering
    \includegraphics[width=\linewidth]{density}
    \captionof{figure}{\label{fig:density} Column density $N$ distribution of HI. It has an asymmetric peak near the galactic plane of $b_{eff} = \ang{0}$, reaching $\ang{-24}$ in the negative spatial direction and $\ang{35}$ in the positive one. It probably corresponds to the galactic disc. The slope is flatter on the positive side. The peak is at $(1.6 \times 10 ^{37} \pm 8\%)\si{\centi\metre^{-2}}$. The high-energy object identified in Fig \ref{fig:map} is probably part of it. There is also a doubtful minor bump of $(4.0 \times 10 ^{35} \pm 8\%)\si{\centi\metre^{-2}}$ near the cloud in Fig \ref{fig:map} at $l = \ang{310}$, $b \in [\ang{30}, \ang{48}]$. The curve is mostly near zero elsewhere, including at the velocity anomaly near $\ang{57}$ examined in \ref{fig:57}, so that is probably not a physical object.}
\end{Figure}

\section{Conclusion}
In this project, we use the dish at Leuschner Observatory to survey the HI frequency range on galactic longitudes $\ang{130}$ and $\ang{310}$, which connect to form a circle. We have roughly mapped out the velocity and column density distribution along them, identifying a main feature mostly uniform in position and frequency, brightest near the galactic plane at $T_B \simeq (90 \pm 7)\si{\kelvin}$ at the highest but also visible at other discrete galactic latitudes, mostly notably $l = \ang{310}$, $b \in [\ang{30}, \ang{48}]$ around $T_B \simeq (23 \pm 7)\si{\kelvin}$. There is an overall trend maximised near $(\ang{130}, \ang{50})$ at $26\si{\kilo\metre\second^{-1}}$ and lower elsewhere with many fluctuations, possibly due to an overall motion of galactic HI. Column density $N$ is mostly non-zero except for an asymmetric peak near the galactic plane, with its galactic latitude ranging from $\ang{-24}$ to $\ang{35}$.

A feature at $\nu \simeq 1420.65\si{\mega\hertz}$ is narrow in height (spanning $\ang{18}$), wide in frequency (spanning $0.5\si{\mega\hertz}$), and distinct from the main trend in both. It is also the brightest feature at $T_B \simeq (104 \pm 7)\si{\kelvin}$ at maximum. It looks like a narrow high-energy object receding rapidly at $-42\si{\kilo\metre\second^{-1}}$ near the known position of the receding star cluster NGC 663, but we are unconfident in any connection. We also see a negative velocity near $(\ang{130}, \ang{53})$ that is much darker than the brightness error margin of $8\si{\klevin}$ and has a baseline near-zero column density, so it is probably not physical, but a result of faint residual systematics.

Our project is sub-optimal and can use many improvements. E.g., our spatial sampling is not ideal and the pointing limitations of our dish combined with our position dictate that 30\% of the circle is never visible. Pointing separation also barely satisfies the Nyquist criterion. We could have denser pointings $\ang{1.3}$ apart, and rewire our system for a more complete survey. In the future, we can even survey the whole sky instead of just one circle.

Our system is so problematic that we have to discard all data from one of the two polarisations. We will investigate its saturation by and compression of high-power signals, which have made calibration data with noise illogically darker than those without it. Meanwhile, there are also pointing-dependent frequency-periodic ripples in the systematics that are hard to remove cleanly, probably from standing waves forming between the dish and its surroundings. We will try sidestep them by clearing the environment and incorporating data from multiple dishes in different locations.

The Local Oscillator frequencies we use are too close together, limiting the frequency range of the final data. We may have missed features at high velocity, which may have out-of-bound frequencies due to strong Doppler shifts, especially approaching ones, as the theoretical HI line is on the left of our spectra. We could use LO frenquecies that are wider apart but will not make the signal fall off the bandpass filter ranges.

The velocity analysis is also simplistic in using naive maxima, which may have resulted in the suspected non-physical anomaly near $(\ang{130}, \ang{53})$. We could create a more complex algorithm that only take maxima of continuous features. Besides, we can analyse the temperature distribution by carefully fitting the features to Gaussians or Voigt profiles and observing their widths.

\section{Acknowledgements}
Chingham Fong, Giovanni Pecorino, and Kian Shahin are the other members of the group. We always take data together.

\bibliographystyle{plain}
\bibliography{references}
\end{multicols}
\end{document}
